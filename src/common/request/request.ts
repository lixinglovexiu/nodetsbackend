const express = require('express');
const app = express();

const cors = require('cors');
app.use(cors()); //解决跨域

const bodyParser = require('body-parser'); //解析参数
app.use(bodyParser.json()); //json请求
app.use(bodyParser.urlencoded({
    extended: false
})); //表单请求

export {
    express,
    app
}