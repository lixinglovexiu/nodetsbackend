// const db_url = "mongodb://lixing:123456@127.0.0.1:27017/test";
// const db_url = "mongodb://lixing:123456@192.168.1.101:27017/test";
const db_url = "mongodb://47.98.160.10:27017/test";
const options = {
  db: { w: 1, native_parser: false },
  server: {
    poolSize: 5,
    socketOpations: { connectTimeoutMS: 500 },
    auto_reconnect: true
  },
  replSet: {},
  mongos: {}
};
const mongoose = require("mongoose");
mongoose.connect(db_url, options);

mongoose.connection.on("connected", function() {
  console.log("连接成功！");
  console.log("Mongoose connection open to " + db_url);
}); /** * 连接异常 */

mongoose.connection.on("error", function(err: any) {
  console.log("Mongoose connection error: " + err);
  console.log("连接失败！");
}); /** * 连接断开 */

mongoose.connection.on("disconnected", function() {
  console.log("断开连接");
  console.log("Mongoose connection disconnected");
});

export {mongoose}