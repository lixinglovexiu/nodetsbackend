const connOption = {
    host: '47.98.160.10',
    user: 'root',
    password: '38022411li',
    port: '3306',
    database: 'photo_wall',
    connectTimeout: 5000
}

const mysql = require('mysql');
let dataPool = createPool();

function createPool() { //数据库断线重连
    let pool = mysql.createPool({
        ...connOption,
        waitForConnections: true, //当无连接可用时等待
        connectionLimit: 100, //最大连接数
        queueLimit: 0, //最大连接等待数（0为不限制）
    })
    pool.on('error', (err:any) => err.code === 'PROTOCOL_CONNECTION_LOST' && setTimeout(createPool, 2000));
    return pool;
}

export{dataPool}
