import { dataPool } from "../common/db/mysql_conn_config";
import { Result } from "../common/response/result";
import { express } from "../common/request/request";
const router = express.Router();

router.get("/", (req: any, res: any) => {
  dataPool.query("select * from photo", (e: any, r: any) => {
    let result = new Result();
    result.data = r;
    res.json(result);
  });
});

const photoCrud = router;
export { photoCrud };
