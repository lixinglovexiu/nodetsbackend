import {photoCrud}  from './photo/photoCRUD';
import {menu}  from './menu/menu';
import {app} from './common/request/request';


// token验证
app.all('*', (req:any, res:any, next:any) => {
    next();
});

app.use('/photo', photoCrud);
app.use('/menu', menu);

app.listen(80,()=>console.log('服务启动'));

