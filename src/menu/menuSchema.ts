import { mongoose } from "../common/db/mongo_conn_config";
const Schema = mongoose.Schema;
const MenuSchema = new Schema(
  {
    title: String,
    href: String,
    children: [{ title: String, href: String }]
  },
  { timestamps: true }
);
const MenuModel = mongoose.model('MenuSchema', MenuSchema);

export {MenuModel}