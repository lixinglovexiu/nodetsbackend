const YAML = require("yamljs");
const nativeObject = YAML.load(
  "/Users/lixing/front_end/NodeTsBackend/src/data/menu.yml"
);
import { express } from "../common/request/request";
const router = express.Router();

router.get("/", (req: any, res: any) => {
  // const jsonstr = JSON.stringify(nativeObject);
  res.json(nativeObject);
});

const menu = router;
export { menu };
